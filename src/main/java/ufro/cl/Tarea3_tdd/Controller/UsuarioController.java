package ufro.cl.Tarea3_tdd.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ufro.cl.Tarea3_tdd.Model.Usuario;
import ufro.cl.Tarea3_tdd.Service.UsuarioService;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "usuarios")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;
    @PostMapping("/crear")
    public ResponseEntity<String> crearUsuario(@RequestBody Usuario usuario) {
        try {
            usuarioService.validarNombre(usuario);
            usuarioService.validarApellidoPaterno(usuario);
            usuarioService.validarApellidoMaterno(usuario);
            usuarioService.validarRut(usuario);
            usuarioService.validarNumeroTelefonico(usuario);
            usuarioService.validarEdad(usuario);


            return ResponseEntity.ok("Usuario creado correctamente!");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
