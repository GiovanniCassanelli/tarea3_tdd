package ufro.cl.Tarea3_tdd.Service;

import org.springframework.stereotype.Service;
import ufro.cl.Tarea3_tdd.Model.Usuario;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UsuarioService {
    public boolean validarNombre(Usuario user) {
        String nombre = user.getNombre();

        if (nombre == null || nombre.trim().isEmpty()) {
            throw new IllegalArgumentException("El nombre no puede estar vacío.");
        }

        String regex = "^[a-zA-Z]+$";
        if (nombre.matches(regex)) {
            System.out.println("Nombre válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El nombre debe contener solo texto.");
        }
    }

    public boolean validarApellidoPaterno(Usuario user) {
        String apellidoPaterno = user.getApellidoPaterno();
        if (apellidoPaterno== null || apellidoPaterno.trim().isEmpty()) {
            throw new IllegalArgumentException("El apellido paterno no puede estar vacío.");
        }
        String regex = "^[a-zA-Z]+$";
        if(user.getApellidoPaterno().matches(regex)){
            System.out.println("Apellido paterno válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El apellido debe contener solo texto");
        }
    }

    public boolean validarApellidoMaterno(Usuario user) {
        String apellidoMaterno = user.getApellidoMaterno();
        if (apellidoMaterno== null || apellidoMaterno.trim().isEmpty()) {
            throw new IllegalArgumentException("El apellido MATERNO no puede estar vacío.");
        }
        String regex = "^[a-zA-Z]+$";
        if(user.getApellidoMaterno().matches(regex)){
            System.out.println("Apellido materno válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El apellido debe contener solo texto");
        }
    }

    public boolean validarRut(Usuario user) {
        String rut = user.getRut();

        if (rut == null || rut.isEmpty()) {
            throw new IllegalArgumentException("El RUT no puede estar vacío.");
        }

        Pattern pattern = Pattern.compile("^[0-9]+-[0-9kK]{1}$");
        Matcher matcher = pattern.matcher(rut);

        if (!matcher.matches()) {
            throw new IllegalArgumentException("Ingrese el formato 11111111-1 de RUT");
        }

        if (rut.length() > 10) {
            throw new IllegalArgumentException("El RUT no puede exceder los 9 dígitos.");
        }

        return true;
    }

    public boolean validarNumeroTelefonico(Usuario user) {
        String numeroTelefonico = user.getNumeroTelefonico();
        if (numeroTelefonico == null || numeroTelefonico.isEmpty()) {
            throw new IllegalArgumentException("El número telefónico no puede estar vacío.");
        }

        Pattern pattern = Pattern.compile("^\\d{9}$");
        Matcher matcher = pattern.matcher(numeroTelefonico);

        if (!matcher.matches()) {
            throw new IllegalArgumentException("Ingrese un número telefónico válido de 9 dígitos.");
        }

        return true;
    }

    public boolean validarEdad(Usuario user) {
        Integer edad = user.getEdad();
        if (edad <= 0) {
            throw new IllegalArgumentException("La edad debe ser un número positivo.");
        }

        if (edad > 150) {
            throw new IllegalArgumentException("La edad ingresada es inválida.");
        }

        return true;
    }
}
