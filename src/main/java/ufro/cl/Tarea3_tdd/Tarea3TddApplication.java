package ufro.cl.Tarea3_tdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea3TddApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea3TddApplication.class, args);
	}

}
