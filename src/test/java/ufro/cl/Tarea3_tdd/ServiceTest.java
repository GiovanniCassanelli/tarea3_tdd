package ufro.cl.Tarea3_tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ufro.cl.Tarea3_tdd.Model.Usuario;
import ufro.cl.Tarea3_tdd.Service.UsuarioService;

public class ServiceTest {
    private Usuario usuario;
    UsuarioService usuarioService;

    @Test
    @DisplayName("Validación nombre")
    public void validarNombre() {
        Usuario user = new Usuario();
        user.setNombre("Giovanni");
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarNombre(user));
    }


    @Test
    @DisplayName("Validación apellido paterno")
    public void validarApellidoPaterno() {
        Usuario user = new Usuario();
        user.setApellidoPaterno("Cassanelli");
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarApellidoPaterno(user));
    }

    @Test
    @DisplayName("Validación apellido materno")
    public void validarApellidoMaterno() {
        Usuario user = new Usuario();
        user.setApellidoMaterno("Pinto");
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarApellidoMaterno(user));
    }
    @Test
    @DisplayName("Validación rut")
    public void validarRut() {
        Usuario user = new Usuario();
        user.setRut("20674182-1");
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarRut(user));
    }

    @Test
    @DisplayName("Validación numero telefonico")
    public void validarNumeroTelefonico() {
        Usuario user = new Usuario();
        user.setNumeroTelefonico("995583470");
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarNumeroTelefonico(user));
    }

    @Test
    @DisplayName("Validación edad")
    public void validarEdad() {
        Usuario user = new Usuario();
        user.setEdad(21);
        UsuarioService userService = new UsuarioService();
        Assertions.assertEquals(true, userService.validarEdad(user));
    }


}
